package cn.net.susan.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 敏感词校验字段
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/20 下午3:21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidSensitiveWordField {
}
