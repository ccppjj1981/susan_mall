package cn.net.susan.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author 苏三
 * @date 2024/8/8 上午10:45
 */
@Configuration
@Component
@ConfigurationProperties(prefix = "aliyun.sms")
@Data
public class AliyunSmsConfig {

    private String host;

    private String signName;

    private String accessKeyId;

    private String accessKeySecret;

    private String registerTemplateCode;

    private String loginTemplateCode;
}
