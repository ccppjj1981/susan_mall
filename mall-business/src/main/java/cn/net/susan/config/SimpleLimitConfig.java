package cn.net.susan.config;

import cn.net.susan.interceptor.SimpleLimitAspect;
import org.springframework.context.annotation.Bean;

/**
 * 简单限流功能配置
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/4/28 上午10:31
 */
public class SimpleLimitConfig {

    @Bean
    public SimpleLimitAspect simpleLimitAspect() {
        return new SimpleLimitAspect();
    }
}
