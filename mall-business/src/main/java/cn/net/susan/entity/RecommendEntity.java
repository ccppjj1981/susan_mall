package cn.net.susan.entity;

import lombok.Data;


/**
 * 推荐实体
 *
 * @author 苏三
 * @date 2024/10/16 下午3:28
 */
@Data
public class RecommendEntity {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 业务ID
     */
    private Long itemId;

    /**
     * 值
     */
    private float value;
}
