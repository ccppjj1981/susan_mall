package cn.net.susan.entity.aftersale;

import cn.net.susan.entity.RequestConditionEntity;
import lombok.Data;
import java.util.List;
import java.math.BigDecimal;

/**
 * 退货单查询条件实体
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-10-28 15:30:56
 */
@Data
public class RefundConditionEntity extends RequestConditionEntity {

   /**
    * ID集合
    */
    private List<Long> idList;

	/**
	 *  ID
     */
	private Long id;
	/**
	 *  订单ID
     */
	private Long tradeId;
	/**
	 *  订单编码
     */
	private String tradeCode;
	/**
	 *  商品ID
     */
	private Long productId;
	/**
	 *  商品名称
     */
	private String name;
	/**
	 *  规格
     */
	private String model;
	/**
	 *  数量
     */
	private Integer quantity;
	/**
	 *  封面图片url
     */
	private String coverUrl;
	/**
	 *  总金额
     */
	private BigDecimal totalAmount;
	/**
	 *  退款金额
     */
	private BigDecimal refundAmount;
	/**
	 *  退货类型 10：退货退款 20：换货
     */
	private Integer refundType;
	/**
	 *  审核状态 10：待审核 20：已同意 30：已拒绝
     */
	private Integer auditStatus;
	/**
	 *  退货状态 10：进行中 20：已拒绝 30：已完成 40：已取消
     */
	private Integer refundStatus;
	/**
	 *  拒绝原因
     */
	private String rejectedReason;
	/**
	 *  创建人ID
     */
	private Long createUserId;
	/**
	 *  创建人名称
     */
	private String createUserName;
	/**
	 *  修改人ID
     */
	private Long updateUserId;
	/**
	 *  修改人名称
     */
	private String updateUserName;
	/**
	 *  是否删除 1：已删除 0：未删除
     */
	private Integer isDel;
}
