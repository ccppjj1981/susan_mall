package cn.net.susan.entity.aftersale.web;

import cn.net.susan.entity.RequestConditionEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 退货查询条件实体
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-10-28 15:33:03
 */
@ApiModel("退货查询条件实体")
@Data
public class RefundConditionWebEntity extends RequestConditionEntity {

    /**
     * 退货类型
     */
    private Integer refundType;
}
