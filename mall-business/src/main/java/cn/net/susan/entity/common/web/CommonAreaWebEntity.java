package cn.net.susan.entity.common.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * web地区实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-10-04 11:43:55
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommonAreaWebEntity {
    /**
     * 系统ID
     */
    private Long id;

    /**
     * 上一级ID
     */
    private Long parentId;

    /**
     * 名称
     */
    private String name;

    /**
     * 拼音
     */
    private String pinyin;

    /**
     * 全称
     */
    private String fullName;

    /**
     * 行政编码
     */
    private String code;

    /**
     * 级别
     */
    private Integer level;
}
