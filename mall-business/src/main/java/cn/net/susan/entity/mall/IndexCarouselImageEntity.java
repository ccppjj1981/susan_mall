package cn.net.susan.entity.mall;

import cn.net.susan.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 首页轮播图实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-08-21 18:34:11
 */
@ApiModel("首页轮播图实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class IndexCarouselImageEntity extends BaseEntity {


	/**
	 * 图片url
	 */
	@ApiModelProperty("图片url")
	private String url;

	/**
	 * 排序
	 */
	@ApiModelProperty("排序")
	private Integer sort;
}
