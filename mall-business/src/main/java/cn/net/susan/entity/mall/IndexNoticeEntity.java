package cn.net.susan.entity.mall;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.net.susan.entity.BaseEntity;

/**
 * 首页公告实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-10-03 15:58:40
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class IndexNoticeEntity extends BaseEntity {


	/**
	 * 标题
	 */
	private String title;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 排序
	 */
	private Integer sort;
}
