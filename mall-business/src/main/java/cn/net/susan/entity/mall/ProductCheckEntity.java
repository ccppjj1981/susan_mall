package cn.net.susan.entity.mall;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 商品检查实体
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/10 上午11:43
 */
@ApiModel("商品检查实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductCheckEntity {

    /**
     * 分类列表
     */
    private List<CategoryEntity> categoryEntities;

    /**
     * 品牌列表
     */
    private List<BrandEntity> brandEntities;
}
