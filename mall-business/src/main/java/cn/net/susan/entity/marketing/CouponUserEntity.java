package cn.net.susan.entity.marketing;

import cn.net.susan.entity.mall.UserProductEntity;
import lombok.Data;

/**
 * 优惠券用户实体
 *
 * @author 苏三
 * @date 2024/9/13 下午6:32
 */
@Data
public class CouponUserEntity extends UserProductEntity {

    /**
     * 优惠券ID
     */
    private Long couponId;

    /**
     * 优惠券名称
     */
    private String couponName;
}
