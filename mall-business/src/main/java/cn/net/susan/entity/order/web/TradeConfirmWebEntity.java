package cn.net.susan.entity.order.web;

import cn.net.susan.entity.shopping.web.DeliveryAddressWebEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单确认web实体
 *
 * @author 苏三
 * @date 2024/9/20 下午3:54
 */
@ApiModel("订单确认web实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TradeConfirmWebEntity {

    /**
     * 收货地址
     */
    @ApiModelProperty("收货地址")
    private DeliveryAddressWebEntity deliveryAddressWebEntity;

    /**
     * 订单明细
     */
    @ApiModelProperty("订单明细")
    private List<TradeItemWebEntity> orderTradeItemList;

    /**
     * 订单优惠券实体
     */
    @ApiModelProperty("订单优惠券实体")
    private TradeCouponWebEntity tradeCouponWebEntity;

    /**
     * 购物车商品集合
     */
    @ApiModelProperty("购物车商品集合")
    private List<TradeConfirmItemReqWebEntity> shoppingCartProductEntityList;

    /**
     * 总金额
     */
    private BigDecimal totalMoney = BigDecimal.ZERO;

    /**
     * 最终支付金额
     */
    private BigDecimal finalMoney = BigDecimal.ZERO;

    /**
     * 优惠金额
     */
    private BigDecimal subtractMoney = BigDecimal.ZERO;

    /**
     * 商品件数
     */
    private int totalCount;

    /**
     * 确认订单code
     */
    private String tradeCode;
}
