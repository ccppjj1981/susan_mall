package cn.net.susan.entity.seckill;

import cn.net.susan.annotation.MaxMoney;
import cn.net.susan.annotation.MinMoney;
import cn.net.susan.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * 秒杀商品实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-07-08 10:57:31
 */
@ApiModel("秒杀商品实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SeckillProductEntity extends BaseEntity {

	/**
	 * 分类名称
	 */
	@ApiModelProperty("分类名称")
	private String categoryName;

	/**
	 * 单位名称
	 */
	@ApiModelProperty("单位名称")
	private String unitName;

	/**
	 * 品牌名称
	 */
	@ApiModelProperty("品牌名称")
	private String brandName;

	/**
	 * 商品名称
	 */
	@ApiModelProperty("商品名称")
	private String name;

	/**
	 * 规格
	 */
	@ApiModelProperty("规格")
	private String model;

	/**
	 * 原价
	 */
	@ApiModelProperty("原价")
	private BigDecimal costPrice;

	/**
	 * 商品ID
	 */
	@NotNull(message = "商品ID不能为空")
	@ApiModelProperty("商品ID")
	private Long productId;

	/**
	 * 预扣库存
	 */
	@NotNull(message = "预扣库存不能为空")
	@MinMoney(value = 0, message = "预扣库存必须大于0")
	@MaxMoney(value = 10000, message = "预扣库存必须小于10000")
	@ApiModelProperty("预扣库存")
	private Integer withHoldQuantity;

	/**
	 * 实际剩余库存
	 */
	@NotNull(message = "实际剩余库存不能为空")
	@MinMoney(value = 0, message = "实际剩余库存必须大于0")
	@MaxMoney(value = 10000, message = "预实际剩余库存必须小于10000")
	@ApiModelProperty("实际剩余库存")
	private Integer remainQuantity;

	/**
	 * 秒杀价格
	 */
	@NotNull(message = "秒杀价格不能为空")
	@MinMoney(value = 0, message = "秒杀价格必须大于0")
	@MaxMoney(value = 10000, message = "秒杀价格必须小于10000")
	@ApiModelProperty("秒杀价格")
	private BigDecimal price;

	/**
	 *  秒杀开始时间
	 */
	@NotNull(message = "秒杀开始时间不能为空")
	@ApiModelProperty("秒杀开始时间")
	private Date startTime;

	/**
	 *  秒杀结束时间
	 */
	@NotNull(message = "秒杀结束时间不能为空")
	@ApiModelProperty("秒杀结束时间")
	private Date endTime;
}
