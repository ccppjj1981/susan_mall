package cn.net.susan.entity.shopping;

import cn.net.susan.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 收货地址实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-09-01 10:02:01
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeliveryAddressEntity extends BaseEntity {


	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 收货人姓名
	 */
	private String receiverName;

	/**
	 * 收货人手机号
	 */
	private String receiverPhone;

	/**
	 * 省份ID
	 */
	private Long provinceId;

	/**
	 * 城市ID
	 */
	private Long cityId;

	/**
	 * 区县ID
	 */
	private Long districtId;

	/**
	 * 省份
	 */
	private String province;

	/**
	 * 城市
	 */
	private String city;

	/**
	 * 区县
	 */
	private String district;

	/**
	 * 邮编
	 */
	private String postCode;

	/**
	 * 详细地址
	 */
	private String detailAddress;

	/**
	 * 是否默认地址 1：是 0：否
	 */
	private Boolean isDefault;
}
