package cn.net.susan.entity.third;

import lombok.Data;

/**
 * 淘宝城市实体
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/2/26 下午2:28
 */
@Data
public class TaoboCityEntity {

    /**
     * 返回码
     */
    private int code;

    /**
     * 信息
     */
    private String msg;

    /**
     * 地区
     */
    private TaoboAreaEntity data;


    @Data
    public static class TaoboAreaEntity {

        /**
         * 国家
         */
        private String country;

        /**
         * 省份
         */
        private String region;

        /**
         * 城市
         */
        private String city;

        /**
         * 查询ip
         */
        private String queryIp;
    }
}
