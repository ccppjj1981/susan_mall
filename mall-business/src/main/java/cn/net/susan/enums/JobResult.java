package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * job执行结果枚举
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/1 上午11:06
 */
@AllArgsConstructor
@Getter
public enum JobResult {

    /**
     * 成功
     */
    SUCCESS(0, "成功"),

    /**
     * 失败
     */
    FAILURE(1, "失败");

    private Integer value;

    private String desc;
}
