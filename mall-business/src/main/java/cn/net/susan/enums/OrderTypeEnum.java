package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单类型
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/7/24 下午4:46
 */
@Getter
@AllArgsConstructor
public enum OrderTypeEnum {

    NORMAL_PRODUCT(1, "普通商品订单"),

    SECKILL_PRODUCT(2, "秒杀商品订单");

    /**
     * 枚举值
     */
    private Integer value;


    /**
     * 枚举描述
     */
    private String desc;
}
