package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 支付状态枚举
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/30 下午3:39
 */
@AllArgsConstructor
@Getter
public enum PayStatusEnum {

    /**
     * 待支付
     */
    WAIT_PAY(1, "待支付"),

    /**
     * 已支付
     */
    PAYMENT(2, "已支付"),

    /**
     * 已退款
     */
    REFUND(3, "已退款"),

    /**
     * 失败
     */
    FAILURE(4, "失败");

    private Integer value;

    private String desc;
}
