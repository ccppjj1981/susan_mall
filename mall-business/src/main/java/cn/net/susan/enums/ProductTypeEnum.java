package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品类型枚举
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/8/15 下午3:39
 */
@AllArgsConstructor
@Getter
public enum ProductTypeEnum {

    /**
     * 热门商品
     */
    HOT(1, "热门商品"),

    /**
     * 新品推荐
     */
    NEW(2, "新品推荐"),

    /**
     * 秒杀商品
     */
    SECKILL(3, "秒杀商品");

    private Integer value;

    private String desc;
}
