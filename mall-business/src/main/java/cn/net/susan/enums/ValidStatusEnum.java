package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 有效状态枚举
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/9/4 下午3:39
 */
@AllArgsConstructor
@Getter
public enum ValidStatusEnum {

    /**
     * 有效
     */
    VALID(1, "有效"),

    /**
     * 无效
     */
    INVALID(0, "无效");

    private Integer value;

    private String desc;
}
