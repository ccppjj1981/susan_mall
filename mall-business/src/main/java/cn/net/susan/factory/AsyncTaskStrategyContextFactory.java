package cn.net.susan.factory;

import cn.net.susan.service.task.IAsyncTask;
import cn.net.susan.util.AssertUtil;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * 定时任务策略工厂
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/6 下午4:28
 */
public class AsyncTaskStrategyContextFactory {

    /**
     * 策略实体映射
     */
    private static Map<Integer, IAsyncTask> asyncTaskMap;

    private static final AsyncTaskStrategyContextFactory INSTANCE = new AsyncTaskStrategyContextFactory();

    private AsyncTaskStrategyContextFactory() {

    }

    public static AsyncTaskStrategyContextFactory getInstance() {
        return INSTANCE;
    }


    /**
     * 初始化策略类
     *
     * @param map 实例映射
     */
    public void initAsyncTaskMap(Map<Integer, IAsyncTask> map) {
        if (MapUtils.isEmpty(asyncTaskMap)) {
            asyncTaskMap = map;
        }
    }


    /**
     * 根据任务类型获取策略实体对象
     *
     * @param taskType 任务类型
     * @return 策略实体对象
     */
    public IAsyncTask getStrategy(Integer taskType) {
        AssertUtil.notNull(taskType, "任务类型不能为空");
        AssertUtil.isTrue(MapUtils.isNotEmpty(asyncTaskMap), "asyncTaskMap不能为空");

        return asyncTaskMap.get(taskType);
    }
}
