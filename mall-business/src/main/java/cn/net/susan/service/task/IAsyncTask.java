package cn.net.susan.service.task;

import cn.net.susan.entity.common.CommonTaskEntity;

/**
 * 定时任务接口
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/6 下午4:32
 */
public interface IAsyncTask {

    /**
     * 执行定时任务
     *
     * @param commonTaskEntity 数据
     */
    void doTask(CommonTaskEntity commonTaskEntity);
}
