package cn.net.susan.constant;

/**
 * key常量
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/7/14 下午7:44
 */
public abstract class KeyConstant {

    private KeyConstant() {

    }

    /**
     * 秒杀商品详情Redis中key的前缀
     */
    public static final String SECKILL_PRODUCT_DETAIL_PFREFIX = "seckillProductDetail:";

    /**
     * 秒杀商品库存
     */
    public static final String SECKILL_PRODUCT_STOCK_PREFIX = "seckillProductStock:";
}
