package cn.net.susan.job;

import cn.net.susan.enums.JobResult;
import cn.net.susan.service.sys.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 数据字典job
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/27 下午5:10
 */
@Slf4j
@Component
public class DictJob extends BaseJob {

    @Autowired
    private DictService dictService;

    @Override
    public JobResult doRun(String params) {
        dictService.refreshDict();
        return JobResult.SUCCESS;
    }
}
