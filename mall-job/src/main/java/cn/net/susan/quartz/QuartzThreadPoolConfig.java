package cn.net.susan.quartz;

import cn.net.susan.config.BusinessConfig;
import cn.net.susan.config.properties.QuartzThreadPoolProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务线程池配置类
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/4/30 下午4:17
 */
@Configuration
public class QuartzThreadPoolConfig {

    @Autowired
    private BusinessConfig businessConfig;


    @Bean("quartzThreadPoolExecutor")
    public ThreadPoolExecutor quartzThreadPoolExecutor() {
        QuartzThreadPoolProperties quartzThreadPoolConfig = businessConfig.getQuartzThreadPoolConfig();
        ThreadPoolExecutor threadPoolTaskExecutor = new ThreadPoolExecutor(quartzThreadPoolConfig.getCorePoolSize(),
                quartzThreadPoolConfig.getMaxPoolSize(),
                quartzThreadPoolConfig.getKeepAliveSeconds(),
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(quartzThreadPoolConfig.getQueueSize()),
                new ThreadPoolExecutor.CallerRunsPolicy());
        return threadPoolTaskExecutor;
    }
}
